//
//  ContactsTableViewController.swift
//  GojekAssignment
//
//  Created by Timotius Leonardo Lianoto on 30/11/20.
//

import UIKit
import CloudKit
import MBProgressHUD

class ContactsTableViewController: UITableViewController {
    var contacts = [CKRecord]()
    var contactsID = [CKRecord.ID]()
    var contactDicTemp = [String:[String]]()
    var contactDictionary = [String:[String]]()
    var sectionTitles = [String]()
    let cloudManager = CloudManager()
    var refresh: UIRefreshControl!
    var selectedContactName = ""
    
    // MARK: - IBOutlet
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        refresh = UIRefreshControl()
        refresh.attributedTitle = NSAttributedString(string: "Pull To Reload Contacts")
        refresh.addTarget(self, action: #selector(loadContacts), for: .valueChanged)
        self.tableView.addSubview(refresh)
        setUpNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadContacts()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        sectionTitles.removeAll()
    }
    
    // MARK: Function
    
    func setUpNavigationBar() {
        let button1 = UIBarButtonItem(image: UIImage(systemName: "plus"), style: .plain, target: self, action: #selector(addButtonPressed))
        self.navigationItem.rightBarButtonItem  = button1
    }
    
    @objc func addButtonPressed() {
        // MARK: APA YANG DILAKUKAN
        performSegue(withIdentifier: "goToEdit", sender: self)
    }
    
    @objc func loadContacts() {
        contacts = [CKRecord]()
        contactsID = [CKRecord.ID]()
        contactDicTemp = [String:[String]]()
        contactDictionary = [String:[String]]()
        self.tableView.reloadData()
        
        let publicData = cloudManager.publicDatabase
        let predicate = NSPredicate(value: true)
        
        let query = CKQuery(recordType: "GojekContacts", predicate: predicate)
        query.sortDescriptors = [NSSortDescriptor(key: "firstName", ascending: false)]
        MBProgressHUD.showAdded(to: self.view, animated: true)
        publicData.perform(query, inZoneWith: nil) { (result, error) in
            if let items = result {
                self.contacts = items
                DispatchQueue.main.async {
                    for item in self.contacts {
                        self.contactsID.append(item.recordID)
                    }
                    for name in items {
                        let firstNameContact = name["firstName"] as? String
                        let lastNameContact = name["lastName"] as? String
                        let mixedNameContact = "\(firstNameContact ?? String()) \(lastNameContact ?? String())"
                        let characterset = ["A","B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
                        var state = 0
                        for char in characterset {
                            if mixedNameContact.prefix(1).uppercased() == char{
                                state = 1
                            }
                        }
                        var contactsKey = ""
                        if state == 0 {
                            contactsKey = "#"
                        }else {
                            contactsKey = mixedNameContact.prefix(1).uppercased()
                        }
                        if var contactsValue = self.contactDictionary[contactsKey] {
                            let contactsUppercased = mixedNameContact.capitalized
                            contactsValue.append(contactsUppercased)
                            self.contactDictionary[contactsKey] = contactsValue
                        }else {
                            self.contactDictionary[contactsKey] = [mixedNameContact]
                        }
                    }
                    self.sectionTitles = [String](self.contactDictionary.keys)
                    self.sectionTitles = self.sectionTitles.sorted(by: {$0 < $1})
                    for value in self.contactDictionary.keys {
                        self.contactDictionary[value] = self.contactDictionary[value]?.sorted(by: {$0 < $1})
                    }
                    self.contactDicTemp = self.contactDictionary
                    self.tableView.reloadData()
                    self.refresh.endRefreshing()
                    self.view.layoutSubviews()
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? DetailsViewController {
            destination.contacts = self.contacts
            destination.selectedContacts = self.selectedContactName
        }else if segue.identifier == "goToEdit" {
            let destination = segue.destination as? EditContactsViewController
            destination?.state = "create"
            destination?.contacts = self.contacts
        }
    }
    
    @IBAction func unwindToContactList(_ unwindSegue: UIStoryboardSegue) {
        // Use data from the view controller which initiated the unwind segue
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sectionTitles.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let contactKey = sectionTitles[section]
        if let contactValues = contactDictionary[contactKey] {
            return contactValues.count
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? ContactsTableViewCell else {
            fatalError("Can't find OwnedBeaconTableViewCell")
        }
        if contacts.count == 0 {
            return cell
        }
        let contactKey = sectionTitles[indexPath.section]
        if  let contactValues = contactDictionary[contactKey] {
            cell.contactNameLabel.text = contactValues[indexPath.row]
            for item in contacts {
                let firstNameContact = item["firstName"] as? String
                let lastNameContact = item["lastName"] as? String
                if contactValues[indexPath.row] == "\(firstNameContact ?? String()) \(lastNameContact ?? String())" {
                    let favoriteState = item["favoriteContact"] as? Int64
                    if favoriteState == 1 {
                        cell.favoriteImageView.isHidden = false
                    }else {
                        cell.favoriteImageView.isHidden = true
                    }
                    
                    if let asset = item["profilePicture"] as? CKAsset, let data = try? Data(contentsOf: asset.fileURL!)
                    {
                        DispatchQueue.main.async
                        {
                            cell.contactImageView.image = UIImage(data: data)
                        }
                    }
                    
                }
            }
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionTitles[section]
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return sectionTitles
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        
    }
    
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return false
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let indexPath = tableView.indexPathForSelectedRow else {
            fatalError("indexpath tidak bisa didapat")
        }
        
        let currentCell = (tableView.cellForRow(at: indexPath)!) as? ContactsTableViewCell
        selectedContactName = currentCell?.contactNameLabel.text ?? "nil"
        
        performSegue(withIdentifier: "goToDetails", sender: self)
    }
}
