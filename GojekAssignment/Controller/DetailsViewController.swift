//
//  ViewController.swift
//  GojekAssignment
//
//  Created by Timotius Leonardo Lianoto on 30/11/20.
//

import UIKit
import CloudKit
import MessageUI

class DetailsViewController: UIViewController, SendDataFromContainerToMaster {
    var contacts = [CKRecord]()
    var selectedContacts = String()
    var currentIdentifier = String()
    var currentContact: CKRecord!
    var containerViewController: DetailsTableViewController?
    var mobileNumber = String()
    var emailAddress = String()
    let cloudManager = CloudManager()
    
    
    @IBOutlet var contactImageView: UIImageView!
    @IBOutlet var contactNameLabel: UILabel!
    @IBOutlet var detailContainerView: UIView!
    @IBOutlet var favouriteButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBar()
        searchForSelectedContact()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        emailAddress = currentContact["emailAddress"] as? String ?? String()
        mobileNumber = currentContact["mobileNumber"] as? String ?? String()
        
        SetUpView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailContainerView" {
            containerViewController = segue.destination as? DetailsTableViewController
            containerViewController!.sendDataFromContainerToMaster = self
        }else if segue.identifier == "goToEditContact" {
            let destination = segue.destination as? EditContactsViewController
            destination?.selectedContact = self.selectedContacts
            destination?.contacts = self.contacts
        }
    }
    
    func changeLabel(text: String) {
        // nothing
    }
    
    func showToast(controller: UIViewController, message: String, seconds: Double) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.backgroundColor = UIColor.black
        alert.view.alpha = 0.6
        alert.view.layer.cornerRadius = 15
        
        controller.present(alert, animated: true)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds){
            alert.dismiss(animated: true)
        }
    }
    
    fileprivate func SetUpView() {
        if let asset = currentContact["profilePicture"] as? CKAsset, let data = try? Data(contentsOf: asset.fileURL!)
        {
            DispatchQueue.main.async
            {
                self.contactImageView.image = UIImage(data: data)
            }
        }
        currentIdentifier = currentContact.recordID.recordName
        self.contactNameLabel.text = "\(currentContact["firstName"] as? String ?? String()) \(currentContact["lastName"] as? String ?? String())"
        containerViewController?.emailLabel.text = currentContact["emailAddress"] as? String
        containerViewController?.mobilePhoneLabel.text = currentContact["mobileNumber"] as? String
        if currentContact["favoriteContact"] as? Int == 1{
            favouriteButton.isSelected = false
        }else {
            favouriteButton.isSelected = true
        }
    }
    
    fileprivate func searchForSelectedContact() {
        for item in contacts {
            let firstNameContact = item["firstName"] as? String
            let lastNameContact = item["lastName"] as? String
            let mixedName = "\(firstNameContact ?? String()) \(lastNameContact ?? String())"
            if selectedContacts == mixedName {
                currentContact = item
            }
        }
    }
    
    @objc func editButtonPressed() {
        performSegue(withIdentifier: "goToEditContact", sender: self)
    }
    
    @objc func backButtonPressed() {
        performSegue(withIdentifier: "unwindToContactList", sender: self)
    }
    
    func setUpNavigationBar() {
        let button1 = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(editButtonPressed))
        self.navigationItem.rightBarButtonItem  = button1
        
        let backButton = UIBarButtonItem(title: "Contacts", style: .plain, target: self, action: #selector(backButtonPressed))
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    @IBAction func unwindToDetails(_ unwindSegue: UIStoryboardSegue) {
        let sourceViewController = unwindSegue.source
        // Use data from the view controller which initiated the unwind segue
    }

    @IBAction func messageButtonPressed(_ sender: Any) {
        if (MFMessageComposeViewController.canSendText()) {
                    let controller = MFMessageComposeViewController()
                    controller.body = "Hello World"
                    controller.recipients = [mobileNumber]
                    controller.messageComposeDelegate = self
                    self.present(controller, animated: true, completion: nil)
                }
    }
    
    @IBAction func callButtonPressed(_ sender: Any) {
        guard let number = URL(string: "tel://" + mobileNumber) else { return }
        UIApplication.shared.open(number)
    }
    
    @IBAction func emailButtonPressed(_ sender: Any) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self as? MFMailComposeViewControllerDelegate
            mail.setToRecipients([emailAddress])
            mail.setMessageBody("<h1>Hello World, Welcome to Gojek Tryout.<h1>", isHTML: true)
            present(mail, animated: true)
        } else {
            print("Cannot send email")
        }
        
    }
    
    @IBAction func favouriteButtonPressed(_ sender: Any) {
        if favouriteButton.isSelected {
            favouriteButton.isSelected = false
            self.cloudManager.publicDatabase.fetch(withRecordID: CKRecord.ID(recordName: currentIdentifier)) { (record, error) in
                if let errorMessage = error {
                    print("Record Error: \(errorMessage.localizedDescription)")
                    
                }else {
                    record?.setValue(1, forKey: "favoriteContact")
                    self.cloudManager.publicDatabase.save(record!, completionHandler: { (newRecord, error) in
                        if let errorMessage = error {
                            print("Save Error: \(errorMessage.localizedDescription)")
                        }
                        else {
                            print("Update Saved")
                        }
                    })
                }
            }
            showToast(controller: self, message: "Selected for Favourite", seconds: 1.2)
        }else {
            favouriteButton.isSelected = true
            self.cloudManager.publicDatabase.fetch(withRecordID: CKRecord.ID(recordName: currentIdentifier)) { (record, error) in
                if let errorMessage = error {
                    print("Record Error: \(errorMessage.localizedDescription)")
                    
                }
                else {
                    record?.setValue(0, forKey: "favoriteContact")
                    self.cloudManager.publicDatabase.save(record!, completionHandler: { (newRecord, error) in
                        if let errorMessage = error {
                            print("Save Error: \(errorMessage.localizedDescription)")
                        }
                        else {
                            print("Update Saved")
                        }
                    })
                    
                }
            }
            showToast(controller: self, message: "Unselected from Favourite", seconds: 1.2)
        }
    }
    
    
}

extension DetailsViewController: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
}

