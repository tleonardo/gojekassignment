//
//  EditContactsViewController.swift
//  GojekAssignment
//
//  Created by Timotius Leonardo Lianoto on 30/11/20.
//

import UIKit
import CloudKit
import MBProgressHUD

class EditContactsViewController: UIViewController, ContainerToMaster, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var contacts = [CKRecord]()
    var selectedContact = String()
    var currentIdentifier = String()
    var containerViewController: EditContactTableViewController?
    var cloudManager = CloudManager()
    var state = String()
    
    var email = String()
    var lastName = String()
    var firstName = String()
    var mobile = String()
    var profilePicture: CKAsset!
    var gradientLayer: CAGradientLayer = {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor(named: "mainColor")!, UIColor.black]//Colors you want to add
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
        gradientLayer.frame = CGRect.zero
       return gradientLayer
    }()
    
    @IBOutlet var profilePictureImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        setUpNavigationBar()
        self.view.layer.addSublayer(gradientLayer)
        gradientLayer.frame = self.view.bounds
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    func setUpView() {
        if state == "create" {
            profilePictureImageView.image = UIImage(systemName: "person.circle")
        }else {
            for item in contacts {
                let firstNameContact = item["firstName"] as? String
                let lastNameContact = item["lastName"] as? String
                let mixedName = "\(firstNameContact ?? String()) \(lastNameContact ?? String())"
                if selectedContact == mixedName {
                    currentIdentifier = item.recordID.recordName
                    if let asset = item["profilePicture"] as? CKAsset, let data = try? Data(contentsOf: asset.fileURL!)
                    {
                        DispatchQueue.main.async
                        {
                            self.profilePictureImageView.image = UIImage(data: data)
                        }
                    }
                    // MARK: DELEGATE ke CONTAINER
                    containerViewController?.emailTextField.text = item["emailAddress"] as? String ?? String()
                    
                    containerViewController?.lastNameTextField.text = item["lastName"] as? String ?? String()
                    
                    containerViewController?.firstNameTextField.text = item["firstName"] as? String ?? String()
                    
                    containerViewController?.mobileNumberTextField.text = item["mobileNumber"] as? String ?? String()
                    
                }
            }
        }
    }
    
    func showToast(controller: UIViewController, message: String, seconds: Double) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.backgroundColor = UIColor.black
        alert.view.alpha = 0.6
        alert.view.layer.cornerRadius = 15
        
        controller.present(alert, animated: true)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds){
            alert.dismiss(animated: true)
        }
    }
    
    @objc func afterDonePreseed() {
        self.navigationController?.popToRootViewController(animated: true)
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    @objc func doneButtonPressed() {
        email = containerViewController?.emailTextField.text ?? String()
        lastName = containerViewController?.lastNameTextField.text ?? String()
        firstName = containerViewController?.firstNameTextField.text ?? String()
        mobile = containerViewController?.mobileNumberTextField.text ?? String()
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let data = (profilePictureImageView.image ?? UIImage()).pngData()
        let url = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(NSUUID().uuidString+".dat")
        do {
            try data?.write(to: url!)
        } catch let e as NSError {
            print("Error! \(e)");
            return
        }
        profilePicture = CKAsset(fileURL: url!)
        
        if state == "create" {
            if containerViewController?.emailTextField.text != "" && containerViewController?.mobileNumberTextField.text != "" && containerViewController?.firstNameTextField.text != "" && containerViewController?.lastNameTextField.text != "" {
                let newItem = CKRecord(recordType: cloudManager.recordType)
                newItem["firstName"] = firstName
                newItem["lastName"] = lastName
                newItem["emailAddress"] = email
                newItem["mobileNumber"] = mobile
                newItem["profilePicture"] = profilePicture
                
                cloudManager.publicDatabase.save(newItem) { (savedRecord, error) in
                    
                    if error == nil {
                        print("success")
                        DispatchQueue.main.sync {
                            let _ = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.afterDonePreseed), userInfo: nil, repeats: false)
                        }
                    } else {
                        print("Record Not Saved Because \(error)")
                    }
                    
                }
            }else {
                showToast(controller: self, message: "Please Fill All The Blanks", seconds: 1.2)
            }
        }else {
            let data = (profilePictureImageView.image ?? UIImage()).pngData()
            let url = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(NSUUID().uuidString+".dat")
            do {
                try data?.write(to: url!)
            } catch let e as NSError {
                print("Error! \(e)");
                return
            }
            profilePicture = CKAsset(fileURL: url!)
            
            self.cloudManager.publicDatabase.fetch(withRecordID: CKRecord.ID(recordName: currentIdentifier)) { (record, error) in
                if let errorMessage = error {
                    print("Record Error: \(errorMessage.localizedDescription)")
                    
                }
                else {
                    record?.setValue(self.firstName, forKey: "firstName")
                    record?.setValue(self.lastName, forKey: "lastName")
                    record?.setValue(self.mobile, forKey: "mobileNumber")
                    record?.setValue(self.email, forKey: "emailAddress")
                    record?.setValue(self.profilePicture, forKey: "profilePicture")
                    self.cloudManager.publicDatabase.save(record!, completionHandler: { (newRecord, error) in
                        if let errorMessage = error {
                            print("Save Error: \(errorMessage.localizedDescription)")
                        }
                        else {
                            print("Update Saved")
                            DispatchQueue.main.sync {
                                let _ = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.afterDonePreseed), userInfo: nil, repeats: false)
                            }
                        }
                    })
                }
            }
        }
    }
    
    @objc func cancelButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editContactTableView" {
            self.containerViewController = segue.destination as? EditContactTableViewController
            containerViewController!.ContainerToMaster = self
        }
    }
    
    func changeLabel(text: String) {
        // do nothing
    }
    
    func setUpNavigationBar() {
        let button1 = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelButtonPressed))
        self.navigationItem.leftBarButtonItem  = button1
        
        let button2 = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneButtonPressed))
        self.navigationItem.rightBarButtonItem  = button2
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            let correctImage = fixOrientation(image: image)
            profilePictureImageView.image = correctImage
        }else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            let correctImage = fixOrientation(image: image)
            profilePictureImageView.image = correctImage
        }
        else {
            fatalError("No Image can be imported")
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func changePictureButtonPressed() {
        let actionSheet = UIAlertController(title: "Please choose how you want to add your image", message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Take From Camera", style: .default, handler: { (camera) in
            // AMBIL DARI KAMERA
            let imagePickerontroller = UIImagePickerController()
            imagePickerontroller.delegate = self
            imagePickerontroller.sourceType = .camera
            imagePickerontroller.allowsEditing = true
            self.present(imagePickerontroller, animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Take From Photos", style: .default, handler: { (photos) in
            // AMBIL DARI PHOTO
            let imagePickerontroller = UIImagePickerController()
            imagePickerontroller.delegate = self
            imagePickerontroller.sourceType = .photoLibrary
            imagePickerontroller.allowsEditing = true
            self.present(imagePickerontroller, animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func fixOrientation(image : UIImage) -> UIImage {
            if image.imageOrientation == .up {
                return image
            }
            var transform = CGAffineTransform.identity
            switch image.imageOrientation {
            case .down,.downMirrored:
                transform = transform.translatedBy(x: image.size.width, y: image.size.height)
                transform = transform.rotated(by: CGFloat(Float.pi))
            case .left,.leftMirrored:
                transform = transform.translatedBy(x: image.size.width, y: 0)
                transform = transform.rotated(by: CGFloat(Float.pi/2))
     
            case .right,.rightMirrored:
                transform = transform.translatedBy(x: 0, y: image.size.height)
                transform = transform.rotated(by: CGFloat(-Float.pi/2))
           
            default:
                break
            }
            
            
            switch image.imageOrientation {
            case .upMirrored, .downMirrored:
                transform = transform.translatedBy(x: image.size.width, y: 0)
                transform = transform.scaledBy(x: -1, y: 1)
            case .leftMirrored, .rightMirrored:
                transform = transform.translatedBy(x: image.size.height, y: 0)
                transform = transform.scaledBy(x: -1, y: 1)
            default:
                break
            }
            
            
            let ctx = CGContext(data: nil, width: Int(image.size.width), height: Int(image.size.height), bitsPerComponent: image.cgImage!.bitsPerComponent, bytesPerRow: 0, space: image.cgImage!.colorSpace!, bitmapInfo: image.cgImage!.bitmapInfo.rawValue)
            ctx!.concatenate(transform)
            switch image.imageOrientation {
            case .left,.leftMirrored,.rightMirrored,.right:
                ctx?.draw(image.cgImage!, in: CGRect(x :0,y:0,width:image.size.height,height: image.size.width))
                
            default:
                 ctx?.draw(image.cgImage!, in: CGRect(x :0,y:0,width:image.size.width,height: image.size.height))
            }
            let cgimg = ctx!.makeImage()
            let img = UIImage(cgImage: cgimg!)
            return img
        }
     
     
    
}
