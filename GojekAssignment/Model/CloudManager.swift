//
//  CloudManager.swift
//  Keeper
//
//  Created by David Christian on 13/10/20.
//

import Foundation
import CloudKit

class CloudManager {
    
    let publicDatabase = CKContainer.init(identifier: "iCloud.com.academy.GojekAssignment").publicCloudDatabase
    let recordType = "GojekContacts"
    
    func read(completionHandler: @escaping (_ record: CKRecord) -> Void) {
        let predicate = NSPredicate(value: true)
        
        let query = CKQuery(recordType: "GojekContacts", predicate: predicate)
        query.sortDescriptors = [NSSortDescriptor(key: "firstName", ascending: false)]
        let operation = CKQueryOperation(query: query)
        
        operation.recordFetchedBlock = { record in
            completionHandler(record)
        }
        publicDatabase.add(operation)
    }
}
